+++
Tags = [ "old", "blog" ]
categories = [ "old", "blog" ]
keywords = [ "old", "blog" ]

menu = "main"
comments = true

thumbnailImagePosition = "left"
autoThumbnailImage = true
coverMeta = "in"

date = "2016-11-12"
title = "Older blog posts"
disqusIdentifier = "deadbeef"
Description = ""
+++

I used to blog via Blogger. Rather than copy content here I'm linking to my Blogger posts for now.

<!--more-->

* [I finially figured out Hiera again](http://justalittlescript.blogspot.com/2014/05/i-finally-figured-out-hieraagain.html)
* [Monitorama 2014](http://justalittlescript.blogspot.com/2014/05/monitorama-2014-predicted-winners.html)
* [VMworld 2013](http://justalittlescript.blogspot.com/2013/09/vmworld-2013.html)
* [VMworld 2011](http://justalittlescript.blogspot.com/2013/09/old-news-vmworld-2011.html)
* [Gnus is awesome](http://justalittlescript.blogspot.com/2008/10/gnus-is-awesome.html)
* [It's easier than you think](http://justalittlescript.blogspot.com/2008/04/its-easier-than-you-think.html)

