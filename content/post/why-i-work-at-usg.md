+++
Categories = ["job", "happy"]
keywords = ["job", "happy"]
Tags = ["usg.edu", "sysadmin", "job", "happy", "career"]

menu = "main"
comments = true

thumbnailImagePosition = "left"
autoThumbnailImage = true
coverMeta = "in"

date = "2017-01-22"
title = "Why I work for usg.edu"
disqusIdentifier = "deadbeef"
#coverImage = "/usg.png"
thumbnailImage = "/usg.png"
#coverCaption = "usg.edu"
Description = ""

+++
<!-- toc -->

Here is why I work for [The University System of Georgia](http://www.usg.edu/)

# tl;dr
For the tl;dr version, read the table of contents.

<!--more-->
# Dumb luck
First things first. I had no Master Plan in the beginning. I do have
"The Plaksin Luck" and credit it for many things, including starting
me down the right career path. I came
to [uga.edu](https://www.uga.edu/) to get a Masters
in [Artificial Intelligence](http://www.ai.uga.edu/). I never finished
that degree but the AI Program exposed me to four of my life-long
loves:

1. [Unix](https://en.wikipedia.org/wiki/Unix) -- AI had Solaris.
2. [Linux](https://en.wikipedia.org/wiki/Linux) -- Linux had just been invented.
3. [Emacs](https://en.wikipedia.org/wiki/Emacs) -- Still waiting for the Emacs Phone
4. [System administration](https://en.wikipedia.org/wiki/System_administrator) and what is now known as [DevOps](https://en.wikipedia.org/wiki/DevOps)

Unix was awesome. I loved the command-line and still do. [The Unix
Philosophy](https://en.wikipedia.org/wiki/Unix_philosophy) shaped my
view of the world even though I had no idea what it was at the time.

It was thrilling to think I could have something like Unix on my
personal computer. One 386-40 and one stack of floppies later, I was
running Linux and Emacs at home. Linux' lack of polish at the time was
perfect for me--I was forced to learn many gory details just to keep
the system running.

AI led to an assistantship
at
[UGA's Helpdesk](https://eits.uga.edu/). The
[web](https://en.wikipedia.org/wiki/History_of_the_World_Wide_Web) was
taking off around this same time. Not knowing any better, when
somebody asked me to make a shopping cart system I wrote one in Bash!
It was never used. The concept of a shopping cart in Bash still makes me
laugh. No doubt it was *completely* secure!

The luckiest people got AI assistantships doing system
administration. They ran AI's Unix systems. They seemed like wizards
that knew everything. Although I later learned they probably knew
nothing about system administration before coming to AI.

The Helpdesk was near the Unix group. Lucky for me I was soon working
in the Unix group. And I am still doing sysadmin today. Thanks, AI
Program!

## Diversion: Emacs phone?
I'm not the first to say that! Here's a _1997_ quote from
the
[MIT Wearable Computing FAQ](http://www.media.mit.edu/wearables/lizzy/FAQ/FAQ.txt):

> Most of the time spent using the wearable is found inside a text
> editor - normally emacs.

That document is full of great quotes! Here's another:

> You can be reading email and still be able to walk down the street
> without running into people.

One more:

> With Internet access, communication becomes very easy - everything
> from real-time messaging ([zephyrs](http://zephyr-im.org/)) to full
> web access using Netscape is commonplace.

# The mission
Working in education makes the mission simple, obvious, and instantly
meaningful. It was not always conscious but I'm sold!

Working for a tech start-up, tech giant, or even a medium-sized tech
company is appealing on some level. Mainly because you end up working
with lots of interesting new technology, some of it bleeding edge. At
the biggest tech giants scale is off the charts, and that is just
plain fascinating. You might be *creating* the bleeding edge!

But technology for technology's sake is not as meaningful as
education. Not to take anything away from the positive impact social
media and other technology has had on the world. It's just not obvious
that Project X is going to turn out to be meaningful when it
begins. Plus working in the commercial sector means you have to keep
secrets to some degree. And that is hard.

Some companies seem to be striking a happy medium. And I hope this
business model survives and flourishes. For
example, [GitLab](https://about.gitlab.com/) which has a
great
[handbook](https://about.gitlab.com/handbook/). And
[Raintank](https://raintank.io/) with
their [OpenSaaS approach](http://raintank.io/about/open-source/):

> it’s not about political beliefs. it’s not about cutting costs.
>
> We believe open source is the best way to develop the most awesome
> software for our customers.
>
> Our mission is to provide a fully supported, turnkey offering for our
> customers through our OpenSaaS platform. We want to help them avoid
> lock in, and take back control: hopefully a refreshing change in a
> world filled with proprietary SaaS options.

Of course [Puppet](https://puppet.com/) is also near and dear to my
heart. And they sound like a great company to work for.

# Building blocks and longevity are more *fun*
Staying put, creating a foundation, and building upon that foundation
is more fun than starting over. You may say "self-fulling prophecy!"
or "confirmation bias!" and I accept that those are possibilities. But
I still *believe* that using building blocks to construct an
ever-taller tower is more fun, more challenging, and more interesting.

![Space elevator](http://www.universetoday.com/wp-content/uploads/2008/12/spaceelevator.jpg "You can do it!")

So many people in commercial IT change companies every few years. This
often means they have to start building from scratch again. Especially
if they work for a new start-up. I do see the appeal of building from scratch
once in a while. But I prefer the fun and challenge of bulding for the long term.
[Ship Of Theseus, even](https://en.wikipedia.org/wiki/Ship_of_Theseus)! That is,
it's a fun challenge to replace or upgrade all the parts over time while 
the whole systems is running.

# Stability and family time
State jobs are stable jobs. They may not pay as much as jobs in the
commercial sector but it is very unlikely that your job is simply
going to disappear. And I'm lazy. At least when it comes to certain
things. Traveling, and potentially moving, a lot for work has little
appeal to me. Plus lots of travel consumes time I'd rather be spending
with my wife and children. Maybe this is another form of being
"[wise selfish](http://philosiblog.com/2012/01/29/it-is-important-that-when-pursing-our-own-self-interest-we-should-be-wise-selfish-and-not-foolish-selfish/)".

One could argue that working for USG pays *better* than working in the
commercial sector. At least in the
long-run. [Benefits](http://www.usg.edu/hr/benefits) are great and we
have a [*defined benefit*](http://www.trsga.com/) retirement
plan. Combine those with less stress and the overall "math" might come
out in the state's favor.

Not to say there is no stress. Every IT job has its stressful
moments. There are plenty of deadlines, races to deliver new
technology without breaking the budget, and so on. Plus the University
System is a loose federation and not a dictatorship when it comes to
IT. This requires more nuance and sometimes trickier technology than
a company with a straight-forward management chain which goes all
the way to the top.

# Plenty of scale
Scaling computer systems is a compelling challenge. It is easy to do anything
one time for one user. Doing the same thing many times for many users
is a [whole 'nother level](https://www.youtube.com/watch?v=YJnGRuidOXI)!

Scale requires automation. Automation requires
collaboration. Collaboration requires love. Love requires
empathy. Hey, you just made some DevOps!

University System IT has enough scale and variety to be
interesting. Even if it's not Netflix! We have thousands of servers
and tens of different projects. Much is automated, much remains to be
automated.

![Terse collaborative love](http://sr.photos2.fotosearch.com/bthumb/CSP/CSP752/k20511982.jpg "Together is better")

# Management and people are great
Great management and great people are extremely important. Without
them you work harder instead of smarter. And often endure a shoestring
budget. Shoestring budgets and overwork cost far more than simply
doing things right.

USG has always supported doing things right. Or as right as
possible given real-world constraints. Part of "right" means allowing
technical people enough space to make their own technical
decisions. This makes a huge difference in terms of both morale and
the end result.

The people at USG are great too. Working hard, working peacefully, and
working together towards common goals. It's been this way for a long time.
No doubt thanks to the culture inspired by both leaders and employees over
the years.

I could go on and on. And I struggled with this section the longest. But just
re-read the first sentence of this section: *Great management and great people
are extremely important*. It's a simple and clear point. If you stop to think
about it, you know whether you have that or not. [Ommmmmmm]([https://en.wikipedia.org/wiki/Om)

# I already lived the startup life once upon a time
My startup-like experience was in the mid-90s when I worked
at [UGA](http://www.uga.edu/). I will not go so far as to say that
everybody should have a startup experience. But that experience taught
me many lessons. And I am still reaping the benefits of those lessons
today.

One of the lessons is to avoid startup experiences! They are both
thrilling and exhausting. No matter how skilled, smart, or experienced
you are, it's virtually impossible to
avoid
[imposter syndrome](https://en.wikipedia.org/wiki/Impostor_syndrome). And
my experience began right after I started my first sysadmin job. Gack!

Only in hindsight did I realize that ran myself into the ground
multiple times. At the time I felt like I was fighting the good fight.
And I still believe that I was. I now know this to be false, but it sure
felt like the entire system depended on me giving it as much attention
as possible. And, in the moments everything worked perfectly, it felt
great.

We ran the University's first large-scale email system. And its first
online learning system. The email system, named "Arches", went from
zero to 10,000 users in weeks. We were shocked and
awed. The ["Web Course Tools"](https://en.wikipedia.org/wiki/WebCT)
system saw similar growth.

![WebCT logo](https://www.polyu.edu.hk/ags/itsnews0112a/webct022.gif "Toolbox!")

Arches was based on
IBM's
[DCE](https://en.wikipedia.org/wiki/Distributed_Computing_Environment)/[DFS](https://en.wikipedia.org/wiki/DCE_Distributed_File_System) which
were revolutionary. When they worked. The third hit for "DCE/DFS" is
still
[debugging tools](http://www-01.ibm.com/software/network/dce/support/index.debugtools.html).
WebCT was written by a professor, worked well enough on a small scale,
and had unsurprising problems scaling to thousands of
users. Unsurprising in retrospect anyhow. WebCT ran on Linux which
didn't yet have perfect hardware support.

Arches and WebCT history provides enough fodder for several blog
posts. Maybe I will write them one day. For now, suffice it to say we
were running bleeding edge software for thousands of users in the
1990s and we bled.

![Comedy/tragedy
masks](http://cf.ltkcdn.net/costumes/images/std/95875-300x211-Comedy_tragedy.JPG "Rollercoaster!")

While one of my lessons *is* to avoid repeating the startup-like
experience, and I'm glad to be well supported at USG so that I don't
have to repeat it, I don't regret my Arches/WebCT time. Instead I like
to focus on the lessons I learned and how they continue to apply to my
job in IT. They might sound like cliches but, well, we all have to
learn these things somewhere:

1. Keep your eyes on the prize. This is the end-user or your more
   immediate customers.
1. Make it work, not perfect.
1. Make it as right as possible given the time constraints.
1. Assume large scale.
1. Manual labor doesn't scale.
1. A foolish **_in_**consistency is the hobgoblin of little minds.
1. Take time to make your own (future) life easier. Find, buy, or make
   the tools you need.

Many of these lessons are captured
in
[The Done Manifesto](http://www.manifestoproject.it/bre-pettis-and-kio-stark/).

Just for laughs, the [Wayback Machine](https://archive.org/web/)
nicely
captures
[an early Arches web page](https://web-beta.archive.org/web/19970413141948/www.arches.uga.edu),
[the middle](https://web-beta.archive.org/web/20030208082228/www.arches.uga.edu) and
[the end](https://web-beta.archive.org/web/20060111064827/www.arches.uga.edu) of
Arches' lifespan.

# And you should (do what you love) too!
![Dove](http://www.animalhi.com/thumbnails/detail/20121109/doves%20dove%201920x1200%20wallpaper_www.animalhi.com_16.jpg "Peace.")
