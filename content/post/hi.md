+++
Tags = [ "first", "post" ]
categories = [ "first", "post" ]
keywords = [ "first", "post" ]

menu = "main"
comments = true

thumbnailImagePosition = "left"
autoThumbnailImage = true
coverMeta = "in"

date = "2016-11-14"
title = "Bloggy!"
disqusIdentifier = "deadbeef"
coverImage = "/lama.jpg"
coverCaption = "Dalai Lama"
Description = ""
+++

<!-- toc -->

[Blog blog bloggy blog blog](https://www.amazon.com/Lets-Drive-Elephant-Piggie-Book/dp/1423164822)! Thanks to [Hugo](https://gohugo.io), [Caddyserver](https://caddyserver.com/) and the [Tranquilpeak theme](https://github.com/kakawait/hugo-tranquilpeak-theme).

The next post will be real.

<!--more-->

The Dalai Lama is awesome.

So is tea.
![Pu Erh](/pu-erh.jpg "Pu Erh so yummy")

{{< alert info no-icon >}}
Tranquilpeak theme lets you make alerts. Both without an icon
{{< /alert >}}

{{< alert success >}}
And with an icon
{{< /alert >}}

# Learning
{{< tweet 781788200675057664 >}}


{{< youtube _QvVaZfFDKw >}}

## TODO

- [x] Blog
- [ ] Blog more
- [ ] Finish Noodle

## Syntax highlighting

```ruby
puts Time.now

[1,2,3].each do |n|
  puts n
end
```

```
for i in 1 2 3 4
do
  echo $i
done
```

```xml
<yo>
  <mo>
    <so>
      fo
    </so>
  </mo>
</yo>
```

```javascript
var jojo;
jojo = 4;

function popo(){
  window.location("https://google.com/");
};

popo();

```

# Fin
.
